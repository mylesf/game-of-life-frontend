# Game of Life Frontend

## Basic Description

This is the Frontend part of an online multiplayer version of Game of Life. It allows users to play online in a synchronized view of board.
The Backend is in [another project](https://bitbucket.org/mylesf/game-of-life-backend/).

The client only performs authentication steps to get a proper user ID with HTTP GET/POST methods. Afterwards, all communications with the server relies on WebSocket. It is designed to work with ActionCable of Ruby on Rails. It might also work, after some changes, with other WebSocket servers.

This project is hosted at [gol.myles.hk](https://gol.myles.hk), you can have a look.

## How to Play
For rules of how cells survive to the next round, please refer to [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life). Following are some additional rules and instructions.
* User is assigned a unique ID(integer) with an associated color on start.
* User can click "ASSIGN NEW COLOR" so a new color (together with new user ID) will be assigned.
* Click on any cell will temporarily enliven the cells with the color of the current user. But they will only be submitted and added to the current round after clicking the "SUBMIT" button.
* Dead cells enlivened by their neighbours(not by the player directly) will be given a color that is the average of its live neighbours.
* You might also use the predefined patterns provided.


## Board Display
The board of cells are encoded and transmitted as Base64 encode binary of PNG file. Each cell is represented by a pixel in the image. The position and color of the pixel matches those of the cell. Alpha channel is not used, and pure white(#FFFFFF) means empty(dead) cell.


## Build/Test/Deploy

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# lint code
$ yarn lint

# lint code with fix
$ yarn lintfix

# run end-to-end tests
$ yarn test

# run unit test
$ yarn unit
```

Only E2E testing is provided for now, and you will need to setup the backend server(default to http://localhost:8080) to perform the test.

This project is ready to be deployed to Heroku, no additional addons required.

The Dockerfile included is for development purpose, and you should not deploy it to production.


## Technical Choices

This project is built with Nuxt.js (Vue.js) due to its easy setup and usages, as well as the great flexibility of Vue.js. The application is presented only in a single page as the interface is quite simple and no need for a second page, while I split different parts into Vue components.
Vuetify is chosen as the UI framework simply because of its popularity and community.

WebSocket is chosen for most part of communication with the server, because the client heavily rely on the server (for computation of the next round), and WebSocket is the best choice for its performance due to less overhead involved in this situation.


## Architecture

The whole projected is served in a single page, so there is only one file each in `/layouts` and `/pages` folder. Vue components involved are in `/components`, several images are in `/static`. Also for Nuxt to load the Vue components, there is a `/plugins/custom-components.js` file to include all the components. If you add or remove any, you will need to update this file, too.


## Settings

Some values are read from environmental variables, which you will find in the `env` section of the `nuxt.config.js` config file.


## Todos
* I believe that the performance of this project can still be improved. E.g. `<canvas>` should be faster, but it will need addiontional work to handle the event listeners, etc.

* The predefined patterns are hard-coded, and the images are copied from Wikipedia. It should definitely be moved to the server (like from API) in the future. Also it might be better to allow rotating.

* It is better to mock a backend server so E2E tests does not need to rely on a running real backend server. The unit tests are not complete due to time constrains.