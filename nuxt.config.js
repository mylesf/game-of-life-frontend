module.exports = {
  /*
  ** Environmental variables
  */
  env: {
    backend_host: process.env.BACKEND_HOSTNAME || "http://localhost:8080",
    time_sync_interval: process.env.TIME_SYNC_INTERVAL || 10000
  },
  /*
  ** Headers of the page
  */
  head: {
    title: "Game of Life | By Myles",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Online multiplayer version of Game of Life"
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Exo" }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: "#3B8070" },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        })
      }
    }
  },
  modules: ["@nuxtjs/vuetify", "@nuxtjs/axios"],
  plugins: [{ src: "~plugins/custom-components", ssr: true }]
}
