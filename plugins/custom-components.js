import Vue from "vue"
import users from "~/components/users.vue"
import board from "~/components/board.vue"
import cell from "~/components/cell.vue"
import patterns from "~/components/patterns.vue"
import loading from "~/components/loading.vue"
import customHeader from "~/components/customHeader.vue"
import customFooter from "~/components/customFooter.vue"

const components = {
  users,
  board,
  cell,
  patterns,
  loading,
  customHeader,
  customFooter
}

Object.entries(components).forEach(([name, component]) => {
  Vue.component(name, component)
})
