import test from "ava"
import { Nuxt, Builder } from "nuxt"
import { resolve } from "path"

// We keep a reference to Nuxt so we can close
// the server at the end of the test
let nuxt = null
let window = null

function loaded(retry) {
  return new Promise((resolve, reject) => {
    ;(function wait() {
      if (!window.document.querySelector(".page-loading")) resolve(true)
      else if (retry <= 0) reject()
      setTimeout(() => {
        retry--
        wait()
      }, 500)
    })()
  })
}

// Init Nuxt.js and start listening on localhost:4000
test.before("Init Nuxt.js", async t => {
  const rootDir = resolve(__dirname, "..")
  let config = {}
  try {
    config = require(resolve(rootDir, "nuxt.config.js"))
  } catch (e) {
    console.error(e)
  }
  config.rootDir = rootDir // project folder
  config.dev = false // production build
  config.mode = "universal" // Isomorphic application
  config.modules = ["@nuxtjs/vuetify", "@nuxtjs/axios"] // load plugins
  config.plugins = [{ src: "~plugins/custom-components", ssr: true }]

  config.env = {
    backend_host: process.env.BACKEND_HOSTNAME || "http://localhost:8080",
    time_sync_interval: process.env.TIME_SYNC_INTERVAL || 10000
  }

  nuxt = new Nuxt(config)
  await new Builder(nuxt).build()
  nuxt.listen(4000, "localhost")

  window = await nuxt.renderAndGetWindow("http://localhost:4000/", {
    pretendToBeVisual: true
  })
  t.not(window.document.querySelector(".page-loading"), null)

  console.log("Waiting for page to load")
  t.true(await loaded(10))
})

test("Basic UI rendered", t => {
  const header = window.document.querySelector(".header")
  t.not(header, null)

  const welcome_title = header.querySelector(".v-toolbar__title")
  t.not(welcome_title, null)
  t.not(
    welcome_title.textContent.match(/Welcome to Game of Life, User #\d+/),
    null
  )

  const new_color_button = window.document.querySelector(
    ".v-toolbar .v-btn__content"
  )
  t.not(new_color_button, null)
  t.is(new_color_button.textContent, "Assign new color")

  const patterns = window.document.querySelector(".patterns")
  t.not(patterns, null)

  const users = window.document.querySelector(".users")
  t.not(users, null)

  const board = window.document.querySelector(".board")
  t.not(board, null)

  const footer = window.document.querySelector(".footer")
  t.not(footer, null)
})

test("Board rendered", t => {
  const board = window.document.querySelector(".board")

  // progress bar rendered
  t.not(board.querySelector(".board .v-progress-linear__bar"), null)

  // cells are rendered
  t.true(board.querySelectorAll(".cell").length > 1)
})

test("Predefined patterns rendered", t => {
  const patterns = window.document.querySelector(".patterns")

  // patterns are rendered
  t.true(patterns.querySelectorAll(".pattern").length > 1)
})

test("Online users list rendered", t => {
  const users = window.document.querySelector(".users")

  // users are rendered
  t.true(users.querySelectorAll(".v-list__tile").length >= 1)

  // myself is displayed
  t.is(
    users
      .querySelectorAll(".v-list__tile")[0]
      .querySelector(".v-list__tile__sub-title").textContent,
    "(Me)"
  )
})

test.cb("Cell click", t => {
  const cell = window.document.querySelectorAll("rect.is-available")[0]
  cell.dispatchEvent(new window.Event("click"))

  // this is just a local change without server interactions, thus should not take more than 500ms
  setTimeout(() => {
    t.true(cell.classList.contains("is-clicked"))
    t.end()
  }, 500)
})

test.cb("Assign new color", t => {
  t.plan(1)

  const new_color_button = window.document.querySelector(".v-toolbar .v-btn")
  new_color_button.click()

  const old_color = window.document
    .querySelectorAll(".v-avatar")[0]
    .querySelector("i").style.backgroundColor

  // this needs server interactions. However, loading overlay will not show imediately, so we wrap it with setTimeout()
  setTimeout(() => {
    loaded(10)
      .then(() => {
        const new_color = window.document
          .querySelectorAll(".v-avatar")[0]
          .querySelector("i").style.backgroundColor
        t.not(old_color, new_color)
      })
      .catch(() => {
        t.fail()
      })
      .finally(() => {
        t.end()
      })
  }, 1000)
})

/**
 * TODO:
 * - add more interactive tests
 * - create stub instead of using real backend server
 */

// Close the Nuxt server
test.after("Closing server and window", () => {
  window.close()
  nuxt.close()
})
