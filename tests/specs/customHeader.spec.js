import Vue from "vue"
import { mount } from "@vue/test-utils"
import customHeader from "~/components/customHeader.vue"
import Vuetify from "vuetify"

describe("customHeader.vue", () => {
  let wrapper
  const EventBus = new Vue()
  const $nuxt = {
    $on: (name, fn) => {
      EventBus.$on(name, fn)
    },
    $emit: name => {
      EventBus.$emit(name)
    }
  }

  beforeEach(() => {
    Vue.use(Vuetify)

    wrapper = mount(customHeader, {
      propsData: {
        userId: 12345
      },
      mocks: {
        $nuxt
      }
    })
  })

  test("mounts properly", () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test("renders properly", () => {
    expect(wrapper.html()).toMatchSnapshot()
  })

  test("assign new user", () => {
    EventBus.$on("assign_new_user", data => {
      wrapper.vm.$emit("assign_new_user", data)
    })
    wrapper.find("button").trigger("click")
    expect(wrapper.emitted("assign_new_user")).toBeTruthy()
  })
})
