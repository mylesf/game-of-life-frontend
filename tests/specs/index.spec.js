import Vue from "vue"
import { shallowMount } from "@vue/test-utils"
import index from "~/pages/index.vue"
import Vuetify from "vuetify"

describe("index.vue", () => {
  let factory
  const EventBus = new Vue()
  const $nuxt = {
    $on: name => {
      EventBus.$on(name, () => {
        EventBus.$emit(name + "_received")
      })
    }
  }
  const mockCurrentUser = {
    token: "asefalseifjalsiejf",
    color: "abcd00",
    id: 4
  }

  const mockUserList = [
    {
      color: "1000ab",
      id: 5
    },
    mockCurrentUser
  ]

  const mockInputs = [
    {
      unix_timestamp_millis: 11111111
    }
  ]

  const $axios = {
    $post: () => {
      return new Promise(resolve => {
        resolve(mockCurrentUser)
      })
    },
    $get: () => {
      return new Promise(resolve => {
        resolve(mockCurrentUser)
      })
    }
  }

  beforeEach(() => {
    Vue.use(Vuetify)

    // mock actioncable
    window.ActionCable = {
      createConsumer: () => {
        return {
          disconnect: () => {
            EventBus.$emit("cable_disconnect")
          },
          subscriptions: {
            create: (options, event_listeners) => {
              EventBus.$on("channel_received", event => {
                event_listeners.received({
                  event: event,
                  data: {
                    inputs: mockInputs,
                    users: mockUserList,
                    board: {
                      unix_timestamp: 1000
                    },
                    client_time: new Date().valueOf(),
                    server_time: new Date().valueOf() + 500
                  }
                })
              })
              EventBus.$on("channel_connected", event_listeners.connected)
              EventBus.$on("channel_disconnected", event_listeners.disconnected)
            }
          }
        }
      }
    }

    factory = () =>
      shallowMount(index, {
        EventBus,
        mocks: {
          $nuxt,
          $axios
        },
        stubs: [
          "customHeader",
          "customFooter",
          "loading",
          "patterns",
          "users",
          "v-container",
          "board"
        ],
        methods: {
          assignNewUser: () => {
            return true
          }
        }
      })
  })

  test("mounts properly", () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test("renders properly", () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })

  test("input submit listener", () => {
    const wrapper = factory()

    EventBus.$on("input_submit_received", data => {
      wrapper.vm.$emit("input_submit_received", data)
    })
    EventBus.$emit("input_submit")
    expect(wrapper.emitted("input_submit_received")).toBeTruthy()
  })

  test("receive assign new user request", () => {
    const wrapper = factory()

    EventBus.$on("assign_new_user_received", data => {
      wrapper.vm.$emit("assign_new_user_received", data)
    })
    EventBus.$emit("assign_new_user")
    expect(wrapper.emitted("assign_new_user_received")).toBeTruthy()
  })

  /**
   *
   * TODO: following are unfinished tests which will need to fix the ActionCable mock first
   *
   **/
  test("channel receive inputs_change", () => {
    const wrapper = factory()
    wrapper.setData({ confirmed_inputs: [] })
    EventBus.$emit("channel_received", "inputs_change")

    // expect(wrapper.vm.confirmed_inputs).toEqual(mockInputs)
  })

  test("channel receive board_change", () => {
    const wrapper = factory()
    wrapper.setData({ board: { unix_timestamp: 2222 } })
    EventBus.$emit("channel_received", "board_change")

    // expect(wrapper.vm.board).toEqual({unix_timestamp: 1000})
  })

  test("channel receive online_users_change", () => {
    const wrapper = factory()
    wrapper.setData({ users: [] })
    EventBus.$emit("channel_received", "online_users_change")

    // expect(wrapper.vm.users[0]).toEqual(mockCurrentUser)
  })

  test("channel receive time sync", () => {
    const wrapper = factory()
    wrapper.setData({ time_delta: 0 })
    EventBus.$emit("channel_received", "time")

    // expect(wrapper.vm.time_delta).toBe(500)
  })

  test("channel connect", () => {
    const wrapper = factory()
    wrapper.setData({ cable_connected: false })
    EventBus.$emit("channel_connected")

    // expect(wrapper.vm.cable_connected).toBe(true)
  })

  test("channel disconnect", () => {
    const wrapper = factory()
    wrapper.setData({ cable_connected: true })
    EventBus.$emit("channel_disconnected")

    // expect(wrapper.vm.cable_connected).toBe(false)
  })
})
