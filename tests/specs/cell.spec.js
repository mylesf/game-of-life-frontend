import Vue from "vue"
import { shallowMount } from "@vue/test-utils"
import cell from "~/components/cell.vue"
import Vuetify from "vuetify"

describe("cell.vue", () => {
  const EventBus = new Vue()
  let factory
  const $nuxt = {
    $on: (name, fn) => {
      EventBus.$on(name, fn)
    },
    $emit: name => {
      EventBus.$emit(name)
    }
  }

  beforeEach(() => {
    Vue.use(Vuetify)

    factory = () =>
      shallowMount(cell, {
        mocks: {
          $nuxt
        },
        propsData: {
          cell: {
            x: 1,
            y: 1,
            available: true
          },
          userColor: "ffa000",
          isCellReset: false
        }
      })
  })

  test("mounts properly", () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test("renders properly", () => {
    const wrapper = factory()
    expect(wrapper.html()).toMatchSnapshot()
  })

  test("calls toggle on after click", () => {
    const wrapper = factory()
    EventBus.$on("cell_toggle_on", data => {
      wrapper.vm.$emit("cell_toggle_on", data)
    })

    wrapper.find("rect").trigger("click")
    expect(wrapper.emitted("cell_toggle_on")).toBeTruthy()
  })

  test("calls toggle off after click", () => {
    const wrapper = factory()
    wrapper.setData({ clicked: true })
    EventBus.$on("cell_toggle_off", data => {
      wrapper.vm.$emit("cell_toggle_off", data)
    })

    wrapper.find("rect").trigger("click")
    expect(wrapper.emitted("cell_toggle_off")).toBeTruthy()
  })

  test("render live cell which is unavailable", () => {
    const wrapper = factory()
    wrapper.setData({ is_pattern_hover: true })
    wrapper.setProps({
      cell: {
        available: false
      }
    })
    expect(wrapper.vm.is_pattern_hover).toBeFalsy()
  })

  test("do nothing if clicked cell not available", () => {
    const wrapper = factory()
    wrapper.setProps({
      cell: {
        available: false,
        color: "abcdef"
      }
    })

    EventBus.$on("cell_toggle_on", data => {
      wrapper.vm.$emit("cell_toggle_on", data)
    })
    EventBus.$on("cell_toggle_off", data => {
      wrapper.vm.$emit("cell_toggle_off", data)
    })

    wrapper.find("rect").trigger("click")
    expect(wrapper.emitted("cell_toggle_on")).toBeFalsy()
    expect(wrapper.emitted("cell_toggle_off")).toBeFalsy()
  })

  test("emit pattern_apply if clicked cell with pattern selected", () => {
    const wrapper = factory()
    EventBus.$on("pattern_apply", data => {
      wrapper.vm.$emit("pattern_apply", data)
    })
    wrapper.setData({ is_pattern_hover: true })

    wrapper.find("rect").trigger("click")
    expect(wrapper.emitted("pattern_apply")).toBeTruthy()
  })

  test("reset cell as requested by prop", () => {
    const wrapper = factory()
    wrapper.setData({ clicked: true })
    wrapper.setData({ is_pattern_hover: true })
    wrapper.setProps({ isCellReset: true })
    expect(wrapper.vm.clicked).toBe(false)
    expect(wrapper.vm.is_pattern_hover).toBe(false)
  })

  test("pattern move enliven", () => {
    const wrapper = factory()
    const stubPattern = {
      1: [1]
    }
    wrapper.setData({ is_pattern_hover: false })

    EventBus.$emit("pattern_move", stubPattern)
    expect(wrapper.vm.is_pattern_hover).toBeTruthy()
  })

  test("pattern move away", () => {
    const wrapper = factory()
    const stubPattern = {
      2: [2]
    }
    wrapper.setData({ is_pattern_hover: true })

    EventBus.$emit("pattern_move", stubPattern)
    expect(wrapper.vm.is_pattern_hover).toBeFalsy()
  })

  test("cell hover emits cell_hover event", () => {
    const wrapper = factory()
    EventBus.$on("cell_hover", data => {
      wrapper.vm.$emit("cell_hover", data)
    })
    wrapper.find("rect").trigger("mouseover")
    expect(wrapper.emitted("cell_hover")).toBeTruthy()
  })

  test("pattern clear hover", () => {
    const wrapper = factory()
    wrapper.setData({ is_pattern_hover: true })

    EventBus.$emit("pattern_clear")
    expect(wrapper.vm.is_pattern_hover).toBeFalsy()
  })

  test("pattern apply", () => {
    const wrapper = factory()
    wrapper.setData({ is_pattern_hover: true })
    wrapper.setData({ clicked: false })
    EventBus.$on("cell_toggle_on", data => {
      wrapper.vm.$emit("cell_toggle_on", data)
    })

    EventBus.$emit("pattern_apply")
    expect(wrapper.vm.is_pattern_hover).toBeFalsy()
    expect(wrapper.vm.clicked).toBe(true)
    expect(wrapper.emitted("cell_toggle_on")).toBeTruthy()
  })
})
