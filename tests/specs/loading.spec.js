import Vue from "vue"
import { mount } from "@vue/test-utils"
import loading from "~/components/loading.vue"
import Vuetify from "vuetify"

describe("loading.vue", () => {
  let wrapper

  beforeEach(() => {
    Vue.use(Vuetify)

    wrapper = mount(loading)
  })

  test("mounts properly", () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test("renders properly", () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
