import Vue from "vue"
import { mount } from "@vue/test-utils"
import patterns from "~/components/patterns.vue"
import Vuetify from "vuetify"

describe("patterns.vue", () => {
  const EventBus = new Vue()
  let wrapper
  const $nuxt = {
    $on: (name, fn) => {
      EventBus.$on(name, fn)
    },
    $emit: name => {
      EventBus.$emit(name)
    }
  }

  beforeEach(() => {
    Vue.use(Vuetify)

    wrapper = mount(patterns, {
      propsData: {
        userId: 12345
      },
      mocks: {
        $nuxt
      }
    })
  })

  test("mounts properly", () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test("renders properly", () => {
    expect(wrapper.html()).toMatchSnapshot()
  })

  test("pattern clear", () => {
    wrapper.setData({ selected_index: 10 })
    EventBus.$emit("pattern_clear")
    expect(wrapper.vm.selected_index).toBe(-1)
  })

  test("pattern apply", () => {
    wrapper.setData({ selected_index: 10 })
    EventBus.$emit("pattern_apply")
    expect(wrapper.vm.selected_index).toBe(-1)
  })

  test("pattern click select", () => {
    EventBus.$on("pattern_select", data => {
      wrapper.vm.$emit("pattern_select", data)
    })
    wrapper.setData({ selected_index: -1 })
    wrapper.find(".pattern").trigger("click")

    expect(wrapper.emitted("pattern_select")).toBeTruthy()
    expect(wrapper.vm.selected_index).toBe(0)
  })

  test("pattern click unselect", () => {
    EventBus.$on("pattern_clear", data => {
      wrapper.vm.$emit("pattern_clear", data)
    })
    wrapper.setData({ selected_index: 0 })
    wrapper.find(".pattern").trigger("click")

    expect(wrapper.emitted("pattern_clear")).toBeTruthy()
    expect(wrapper.vm.selected_index).toBe(-1)
  })
})
