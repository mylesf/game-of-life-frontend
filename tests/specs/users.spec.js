import Vue from "vue"
import { mount } from "@vue/test-utils"
import users from "~/components/users.vue"
import Vuetify from "vuetify"

describe("users.vue", () => {
  let wrapper

  beforeEach(() => {
    Vue.use(Vuetify)

    wrapper = mount(users, {
      propsData: {
        userId: 12345,
        users: [
          {
            color: "abcd00",
            id: 12345
          },
          {
            color: "ab00cd",
            id: 54321
          }
        ]
      }
    })
  })

  test("mounts properly", () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test("renders properly", () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
