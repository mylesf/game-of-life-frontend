import Vue from "vue"
import { mount } from "@vue/test-utils"
import customFooter from "~/components/customFooter.vue"
import Vuetify from "vuetify"

describe("customFooter.vue", () => {
  let wrapper

  beforeEach(() => {
    Vue.use(Vuetify)

    wrapper = mount(customFooter)
  })

  test("mounts properly", () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test("renders properly", () => {
    // fixd year so we have fixed snapshot
    const fixed_year_wrapper = mount(customFooter, {
      computed: {
        current_year() {
          return 2018
        }
      }
    })
    expect(fixed_year_wrapper.html()).toMatchSnapshot()
  })
})
