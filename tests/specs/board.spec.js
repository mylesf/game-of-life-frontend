/**
 * @jest-environment jsdom
 */

import Vue from "vue"
import { mount } from "@vue/test-utils"
import board from "~/components/board.vue"
import Vuetify from "vuetify"

describe("board.vue", () => {
  const EventBus = new Vue()
  const stubCell = {
    x: 1,
    y: 2,
    available: true
  }
  const stubPattern = {
    0: [3]
  }
  const stubInputCells = {
    30: [1, 2, 3]
  }

  let factory
  const $nuxt = {
    $on: (name, fn) => {
      EventBus.$on(name, fn)
    },
    $emit: (name, data) => {
      EventBus.$emit(name, data)
    }
  }

  beforeEach(() => {
    Vue.use(Vuetify)

    factory = () =>
      mount(board, {
        mocks: {
          $nuxt
        },
        propsData: {
          board: {
            content:
              "iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAElBMVEX///8NSy+c41jd2cdLZMdVVVXecVs9AAAAAWJLR0QF+G/pxwAAAAd0SU1FB+IKDQg2MxddcY4AAACuSURBVDjLtVKLEYMgDE2CA9BOkHMCRnD/qVqrSGKSSvH6zvM83ifwBOADBA3EBwwid6nwF7Ed0eHDPpGjcn0zS4rEhI2pJ3pKEwfbx82Aa877Sd8qiirQIOXA6wCxPzol5/YJUMKNlXBGcS3rQQ+G1XqrIBtrWsRyHccIpjZub6fQPUEyNIvuAKajSqb9n9qA6hnE0sfy6eZMRko+Q9GduwL9Ua0Q93cjdAApZOgF32kFjaitJ9wAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTgtMTAtMTNUMDg6NTQ6NTErMDA6MDDEG+e9AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE4LTEwLTEzVDA4OjU0OjUxKzAwOjAwtUZfAQAAAABJRU5ErkJggg==",
            unix_timestamp: Math.round(new Date().valueOf() / 1000),
            interval: 5
          },
          confirmedInputs: [],
          userColor: "abcde00",
          timeDelta: 0
        },
        stubs: ["VProgressLinear"]
      })
  })

  test("mounts properly", () => {
    const wrapper = factory()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test("renders properly", () => {
    const wrapper = factory()
    wrapper.setData({ input_cells: stubInputCells })
    wrapper.setData({ pattern: stubPattern })

    expect(wrapper.html()).toMatchSnapshot()
  })

  test("get seconds to next step", done => {
    const wrapper = factory()
    const old_percentage = wrapper.vm.seconds_to_next_step_percentage

    setTimeout(() => {
      expect(wrapper.vm.seconds_to_next_step_percentage <= old_percentage).toBe(
        true
      )
      done()
    }, 1000)
  })

  test("emit input_submit after button click", () => {
    const wrapper = factory()
    EventBus.$on("input_submit", () => {
      wrapper.vm.$emit("input_submit")
    })
    wrapper.setData({ input_cells: stubInputCells })
    expect(wrapper.vm.is_cell_reset).toBe(false)
    wrapper.find("button.submit-btn").trigger("click")
    expect(wrapper.vm.is_cell_reset).toBe(true)
    expect(wrapper.emitted("input_submit")).toBeTruthy()
  })

  test("emit input_clear after button click", () => {
    const wrapper = factory()
    EventBus.$on("input_clear", () => {
      wrapper.vm.$emit("input_clear")
    })
    wrapper.setData({ input_cells: stubInputCells })
    expect(wrapper.vm.is_cell_reset).toBe(false)
    wrapper.find("button.clear-btn").trigger("click")
    expect(wrapper.vm.is_cell_reset).toBe(true)
    expect(wrapper.emitted("input_clear")).toBeTruthy()
  })

  test("pattern move are calculated correctly", () => {
    const wrapper = factory()
    EventBus.$on("pattern_move", data => {
      wrapper.vm.$emit("pattern_move", data)
    })

    EventBus.$emit("pattern_select", stubPattern)
    EventBus.$emit("cell_hover", stubCell)
    const expected_pattern = {
      2: [1]
    }
    const emitted = wrapper.emitted("pattern_move")
    expect(emitted[emitted.length - 1][0]).toEqual(expected_pattern)
  })

  test("pattern_move emit when pattern selected", () => {
    const wrapper = factory()
    EventBus.$on("pattern_move", data => {
      wrapper.vm.$emit("pattern_move", data)
    })
    EventBus.$emit("pattern_select", stubPattern)

    expect(wrapper.emitted("pattern_move")).toBeTruthy()
  })

  test("pattern_move emit when cursor moves on cell", () => {
    const wrapper = factory()
    EventBus.$on("pattern_move", data => {
      wrapper.vm.$emit("pattern_move", data)
    })
    EventBus.$emit("cell_hover", stubCell)

    expect(wrapper.emitted("pattern_move")).toBeTruthy()
  })
})
